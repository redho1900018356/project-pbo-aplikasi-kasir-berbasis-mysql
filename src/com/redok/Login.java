package com.redok;

import java.sql.SQLException;
import java.util.Scanner;

public class Login {
    DBConnect db = new DBConnect();
    private String username, password, role;
    Scanner inputan = new Scanner(System.in);

    public String getUsername() {
        return username;
    }

    public String getRole() {
        return role;
    }

    public void mainLogin() {
        try {
            int percobaan = 0;
            boolean loop = true;
            while (loop){
                System.out.println("=============Login Panel=============");
                System.out.print("Username : ");username = inputan.nextLine();
                System.out.print("Password : ");password = inputan.nextLine();
                db.rs = db.stmt.executeQuery("SELECT * FROM user");
                while (db.rs.next()){
                    if (username.equals(db.rs.getString("username")) && password.equals(db.rs.getString("password"))){
                        System.out.println("Anda berhasil login!\n");
                        role = db.rs.getString("role");
                        loop = false;
                    }
                }
                if (loop){
                    System.out.println("Username/Password salah, coba lagi!\n");
                    percobaan++;
                }
                if (percobaan > 3){
                    loop = false;
                }
            }
            if (percobaan > 3){
                System.out.println("Anda sudah gagal sebanyak 3x, program otomatis akan ditutup");
                System.exit(1);
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }

    }
}
