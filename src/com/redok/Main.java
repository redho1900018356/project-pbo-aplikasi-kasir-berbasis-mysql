package com.redok;
import java.sql.*;

public class Main {

    public static void main(String[] args) {
        Login login = new Login();
        Admin admin = new Admin();
        User user = new User();

        login.mainLogin();

        if (login.getRole().equals("admin")){
                    admin.mainAdmin();
        }else if (login.getRole().equals("user")){
                    user.mainUser();
        }

    }
}
