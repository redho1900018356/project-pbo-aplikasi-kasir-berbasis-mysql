package com.redok;

import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class User {
    DBConnect db = new DBConnect();
    Scanner inputan = new Scanner(System.in);
    private int jumBarang, idTrk, idBrg, qty;
    private String nama;
    private boolean trkSama = false;

    public void mainUser(){
        boolean loop = true;
        while (loop){
            System.out.println("=======Selamat Datang di Aplikasi Kasir=======");
            System.out.println("List barang : ");
            tampilkanBarang();
            System.out.println("\nMenu : ");
            System.out.println("\t 1. Kasir");
            System.out.println("\t 2. Keluar");
            try{
                System.out.print("Pilihan :");int choice = inputan.nextInt();inputan.nextLine();
                switch (choice) {
                    case 1:
                        kasir();
                        break;
                    case 2:
                        System.out.println("Terimakasih sudah menggunakan program....");
                        System.out.println("Program akan otomatis berhenti......");
                        loop = false;
                        break;
                    default:
                        System.out.println("Salah inputan, coba ulang lagi!");
                        break;
                }
            }catch (InputMismatchException e){
                System.out.println("Salah inputan, coba ulang lagi!");
                inputan.nextLine();
            }
        }
    }

    private  void kasir(){
        boolean loopKasir = true;
        String[] namaBrg = new String[100];
        int[] jumBrg = new int[100];
        int hargaBrg = 0, jumTrk = 0, totalHarga = 0;
        boolean lanjut = false;
        while (loopKasir){
            try{
                if (!trkSama) System.out.print("Masukan nama pelanggan : "); nama = inputan.nextLine();
                System.out.print("Masukan kode barang    : "); idBrg = inputan.nextInt();
                System.out.print("Masukan jumlah barang  : "); qty = inputan.nextInt(); inputan.nextLine();
                db.rs = db.stmt.executeQuery("SELECT * FROM barang where id = '"+idBrg+"'");
                while (db.rs.next()){
                    if (db.rs.getInt(1) > 0){
                        lanjut = true;
                    }
                }
                if (lanjut){
                    db.rs = db.stmt.executeQuery("SELECT * FROM barang where id = '"+idBrg+"'");
                    while (db.rs.next()){
                        namaBrg[jumTrk] = db.rs.getString("nama");
                        jumBrg[jumTrk] = qty;
                        jumTrk++;
                        hargaBrg = (db.rs.getInt("harga") * qty);
                        totalHarga += hargaBrg;
                    }
                    if (trkSama){
                        db.stmt.execute("UPDATE transaksi SET total = total + '"+hargaBrg+"' WHERE id = '"+idTrk+"' ");
                    }else{
                        db.stmt.execute("INSERT INTO transaksi (pelanggan, total) VALUES ('"+nama+"', '"+hargaBrg+"')");
                    }
                    db.rs = db.stmt.executeQuery("SELECT * FROM transaksi ORDER BY id DESC LIMIT 1");
                    while (db.rs.next()){
                        idTrk = db.rs.getInt("id");
                    }
                    db.stmt.execute("INSERT INTO detail_transaksi (id_transaksi, id_barang, qty) VALUES ('"+idTrk+"', '"+idBrg+"', '"+qty+"')");
                    System.out.println("Tambah barang? (1 = y, 0 = n)");int choice = inputan.nextInt();
                    if (choice == 1){
                        trkSama = true;
                    }else {
                        System.out.println("List belanjaan anda : ");
                        for (int i = 0; i < jumTrk; i++){
                            System.out.println("\t -"+jumBrg[i]+" buah "+namaBrg[i]);
                        }
                        System.out.println("Total belanjaan anda : "+totalHarga);
                        loopKasir = false;
                        trkSama = false;
                    }
                }else {
                    System.out.println("Silahkan input kode kembali!");
                }
            }catch (SQLException e){
                System.out.println(e.getMessage());
            }
        }
    }

    private void tampilkanBarang(){
        getJumBarang();
        try {
            db.rs = db.stmt.executeQuery("SELECT * FROM barang");
            if (jumBarang > 0){
                while (db.rs.next()){
                    System.out.println("Kode : "+db.rs.getInt("id")+" | "+db.rs.getString("nama")+" - Rp."+db.rs.getInt("harga")+"");
                }
            }else {
                System.out.println("Barang masih kosong!");
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }

    private int getJumBarang() {
        try{
            db.rs = db.stmt.executeQuery("SELECT COUNT(*) as total FROM barang");
            while (db.rs.next()){
                jumBarang = db.rs.getInt(1);
            }
        }
        catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return jumBarang;
    }


}
