package com.redok;

import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Admin {
    DBConnect db = new DBConnect();
    private int jumBarang;
    Scanner inputan = new Scanner(System.in);

    public void mainAdmin(){
        boolean loop = true;
        while (loop){
            try {
                System.out.println("===========Selamat Datang di Menu Admin===========");
                System.out.println("Jumlah barang sekarang : "+getJumBarang());
                tampilkanBarang();
                System.out.println("Menu : ");
                System.out.println("\t 1. Tambah Barang");
                System.out.println("\t 2. Edit Barang");
                System.out.println("\t 3. Hapus Barang");
                System.out.println("\t 4. Keluar");
                System.out.print("Pilihan : ");
                int choice = inputan.nextInt(); inputan.nextLine();
                switch (choice){
                    case 1 : {
                        tambahBarang();
                        break;
                    }
                    case 2 : {
                        editBarang();
                        break;
                    }
                    case 3 : {
                        hapusBarang();
                        break;
                    }
                    case 4 : {
                        System.out.println("Terimakasih sudah menggunakan program....");
                        System.out.println("Program akan otomatis berhenti......");
                        loop = false;
                        break;
                    }
                    default: {
                        System.out.println("Salah inputan, coba ulang lagi!");
                        break;
                    }
                }
            }
            catch (InputMismatchException e){
                System.out.println("Salah inputan, coba ulang lagi!");
                inputan.nextLine();
            }
        }
    }

    private void tampilkanBarang(){
        System.out.println("List barang sekarang : ");
        try {
            db.rs = db.stmt.executeQuery("SELECT * FROM barang");
            if (jumBarang > 0){
                while (db.rs.next()){
                    System.out.println("====================================");
                    System.out.println("\t ID Barang       : "+db.rs.getInt("id"));
                    System.out.println("\t Nama Barang     : "+db.rs.getString("nama"));
                    System.out.println("\t Harga Barang    : Rp."+db.rs.getInt("harga"));
                    System.out.println("\t Stok Barang     : "+db.rs.getInt("stok"));
                    System.out.println("====================================\n");

                }
            }else {
                System.out.println("Barang masih kosong!");
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }

    private int getJumBarang() {
        try{
            db.rs = db.stmt.executeQuery("SELECT COUNT(*) as total FROM barang");
            while (db.rs.next()){
                jumBarang = db.rs.getInt(1);
            }
        }
        catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return jumBarang;
    }

    private void tambahBarang(){
        try {
            System.out.println("====Tambah Barang====");
            System.out.println("Silahkan masukan isi pada kolom dibawah ini : ");
            System.out.print("\t Nama Barang   : ");String nama = inputan.nextLine();
            System.out.print("\t Harga Barang  : ");int harga = inputan.nextInt();
            System.out.print("\t Stok Barang   : ");int stok = inputan.nextInt();
            inputan.nextLine();
            boolean rs = db.stmt.execute("INSERT INTO barang (nama, harga, stok) VALUES ('"+nama+"', '"+harga+"', '"+stok+"')");
            if (!rs){
                System.out.println("Data tidak berhasil ditambah!");
            }else {
                System.out.println("Data telah berhasil ditambah!");
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }

    private void editBarang(){
        System.out.println("====Edit Barang====");
        tampilkanBarang();
        try{
            System.out.print("Pilih ID Barang yang ingin diubah : ");int id = inputan.nextInt();inputan.nextLine();
            System.out.println("*Catatan : Silahkan diisi kembali kolom dibawah ini dengan nilai sebelumnya jika tidak ingin ada perubahan pada kolom tersebut.");
            System.out.print("\t Nama Barang   : ");String nama = inputan.nextLine();
            System.out.print("\t Harga Barang  : ");int harga = inputan.nextInt();
            System.out.print("\t Stok Barang   : ");int stok = inputan.nextInt();
            inputan.nextLine();
            boolean rs = db.stmt.execute("UPDATE barang SET nama = '"+nama+"', harga = '"+harga+"', stok = '"+stok+"' WHERE id = '"+id+"'");
            if (!rs){
                System.out.println("Data tidak berhasil diubah!");
            }else {
                System.out.println("Data telah berhasil diubah!");
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }

    private void hapusBarang(){
        System.out.println("====Edit Barang====");
        tampilkanBarang();
        try{
            System.out.print("Pilih ID Barang yang ingin dihapus : ");int id = inputan.nextInt();inputan.nextLine();
            boolean rs = db.stmt.execute("DELETE FROM barang WHERE id = '"+id+"'");
            if (!rs){
                System.out.println("Data tidak berhasil dihapus!");
            }else {
                System.out.println("Data telah berhasil dihapus!");
            }

        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }
}
