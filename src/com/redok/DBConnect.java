package com.redok;

import java.sql.*;

public class DBConnect {
    public  String host = "jdbc:mysql://localhost:3306/kasir";
    public  String username = "root";
    public  String passowrd = "";
    public  Connection con;
    public  Statement stmt;
    public  ResultSet rs;

    public DBConnect(){
        try{
            con = DriverManager.getConnection(host, username, passowrd);
            stmt = con.createStatement();
        }catch (SQLException err){
            System.out.println(err.getMessage());
        }
    }
}
